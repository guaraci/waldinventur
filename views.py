import json
from datetime import date

from django.conf import settings
from django.contrib.gis.db.models import Union
from django.db.models import Count, Q
from django.views.generic import TemplateView, View
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import gettext as _

from .models import Inventory, Perimeter, Plot


class ManageView(TemplateView):
    """List of inventories to manage or inventory if manager."""
    template_name = 'manager_home.html'

    def get_context_data(self, **context):
        inventories = Inventory.get_for_manager(self.request.user).order_by('-inv_from')
        return {
            **super().get_context_data(**context),
            'title': _(settings.MOBILE_TITLE),
            'inventories': inventories.filter(inv_to__gte=date.today()).order_by('inv_from'),
            'archives': inventories.filter(inv_to__lt=date.today()).order_by('-inv_from'),
        }


class OverviewMap(TemplateView):
    template_name = 'overview.html'


class OverviewMapData(View):
    def get(self, request, *args, **kwargs):
        invents = Inventory.objects.all()
        mpoly = Inventory.objects.all().aggregate(Union('geom'))['geom__union']
        plots_outside = Plot.objects.exclude(center__within=mpoly)
        inv_geojson = {"type": "FeatureCollection", "features": [inv.as_geojson(srid=4326) for inv in invents]}
        plots_geojson = {
            "type": "FeatureCollection",
            "features": [
                as_geojson(plot, srid=4326) for plot in Plot.objects.all(
                    ).annotate(num_obs=Count('plotobs', filter=Q(plotobs__date_created__year__gte=2022)))
            ]
        }
        return JsonResponse({
            'inventories': inv_geojson,
            'plots': plots_geojson,
            'plots_outside': list(plots_outside.values_list('pk', flat=True)),
        })


def as_geojson(plot, srid=None):
    """A version of plot.as_geojson with less data but num_obs field."""
    if (srid is not None and srid != plot.center.srid):
        plot.center.transform(srid)
    geojson = {
        "type": "Feature",
        "id": plot.pk,
        "properties": {
            "id": plot.pk,
            "nr": plot.nr,
            "num_obs": plot.num_obs,
        },
        "geometry": json.loads(plot.center.json),
    }
    return geojson
