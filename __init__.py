from os.path import dirname

from django.apps import apps


def load_tests(loader, tests, pattern):
    if apps.is_installed("waldinventur"):
        return loader.discover(start_dir=dirname(__file__), pattern=pattern)
