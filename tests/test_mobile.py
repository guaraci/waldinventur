import json
import time
from datetime import date, timedelta

from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

from mobile_base.tests.base import MobileSeleniumTestsBase
from waldinventur.models import (
    Inventory, Municipality, Plot, PlotObs, Tree, TreeObs, TreeSpecies, Vita
)

this_year = date.today().year


class ProjectDataMixin:
    @classmethod
    def setUpTestData(cls):
        cls.user = get_user_model().objects.create_user(username='user', password='secret')
        plot_point = Point(2573950, 1230600, srid=2056)
        cls.municipality = Municipality.objects.get(name='Tramelan')
        cls.inventory = Inventory.objects.create(
            name="Test inventory",
            geom=cls.municipality.perim[0],
            inv_from=date.today(),
            inv_to=date.today() + timedelta(days=30),
        )
        cls.inventory.operators.add(cls.user)
        cls.plot = Plot.objects.create(
            nr=1, sealevel=763, slope=70,
            center=plot_point,
        )
        TreeSpecies.objects.bulk_create([
            TreeSpecies(code="BU", description="Buche"),
            TreeSpecies(code="TA", description="Tanne"),
            TreeSpecies(code="FI", description="Fiechte"),
            TreeSpecies(code="UL", description="Übrige Laubholz"),
        ])


class MobileSeleniumTestsWaldinventur(ProjectDataMixin, MobileSeleniumTestsBase):
    fixtures = ['municipalities_test', 'vita']

    def new_observation(self):
        # Click on current year button
        self.selenium.find_element(By.CLASS_NAME, 'new').click()
        self.selenium.find_element(By.ID, "input-start").click()
        ####### STEP 1 ########
        slope_input = self.selenium.find_element(By.ID, "id_slope")
        self.assertEqual(
            float(self.selenium.find_element(By.ID, "id_forest_edgef").get_attribute('value')),
            1.0
        )
        self.assertEqual(slope_input.get_attribute('value'), "70")
        self.assertEqual(
            self.selenium.find_element(By.ID, "plot-radius").text,
            "10.8m"
        )
        # Changing the slope is immediately synchronized with radius
        slope_input.clear()
        slope_input.send_keys("30")
        self.selenium.find_element(By.ID, "id_forest_edgef").click()
        self.assertEqual(
            self.selenium.find_element(By.ID, "plot-radius").text,
            "9.98m"
        )
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step1"]//button[@type="submit"]'))
        ).click()
        ####### STEP 2 ########
        # Freeze current coordinates from GPS
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "currentLong"), '.')
        )
        self.selenium.find_element(By.ID, "keepthis").click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step2"]//button[@type="submit"]'))
        ).click()
        self.selenium.find_element(By.ID, "id_remarks").send_keys("Une belle remarque de placette")
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step3"]//button[@type="submit"]'))
        ).click()
        ####### Tree forms #######
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="stepfinal"]//button[@class="newtree"]'))
        ).click()
        tree_form = self.selenium.find_element(By.ID, "steptree")
        self.assertTrue(tree_form.find_element(By.ID, "id_species").is_enabled())
        # Next number is automatically attributed
        self.assertEqual(tree_form.find_element(By.ID, "id_nr").get_attribute('value'), "1")
        Select(tree_form.find_element(By.ID, "id_species")).select_by_visible_text("Tanne")
        tree_form.find_element(By.ID, "id_distance").send_keys("24")
        tree_form.find_element(By.ID, "id_azimuth").send_keys("320")
        tree_form.find_element(By.ID, "id_dbh").send_keys("14")
        tree_form.find_element(By.ID, "id_marked").click()
        tree_form.find_element(By.ID, "id_remarks").send_keys("Remarque.")
        self.assertFalse(self.selenium.find_element(By.ID, "id_height").is_displayed())
        Select(tree_form.find_element(By.ID, "id_vita")).select_by_visible_text("Probebaum ist ein Dürrständer (m)")
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="steptree"]//button[@type="submit"]'))
        ).click()

        # New form tree triggered by the caliper
        self.send_caliper_values(species='TA', diameter=277)
        tree_form.find_element(By.ID, "id_distance").send_keys("97")
        tree_form.find_element(By.ID, "id_azimuth").send_keys("280")

        # If at that stage, a new caliper measure arrives, a dialog/alert should popup
        self.send_caliper_values(species='FI', diameter=180)
        self.selenium.find_element(By.ID, "confirm-cancel").click()
        # diameter is always floored, not rounded
        self.assertEqual(tree_form.find_element(By.ID, "id_dbh").get_attribute('value'), "27")

        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="steptree"]//button[@type="submit"]'))
        ).click()

        # Send some random bluetooth value, as it can happen in real.
        self.send_caliper_values(line='$PHGF,ID,41040,26,LI')
        self.send_caliper_values(line='NETAX,10,*62')
        # 2nd tree triggered by the caliper (combined distance/diameter values)
        self.send_caliper_values(species='TA', diameter=141, distance=465)
        tree_form.find_element(By.ID, "id_azimuth").send_keys("380")
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="steptree"]//button[@type="submit"]'))
        ).click()

        # It's possible to go back to an already handled tree and see the entered values
        self.selenium.find_element(By.XPATH, value='//*[@id="trees_summary"]/tbody/tr[1]/td[1]').click()
        self.assertTrue(self.selenium.find_element(By.ID, "canceltree").is_displayed())
        self.assertEqual(
            self.selenium.find_element(By.ID, "id_dbh").get_attribute('value'), "14")
        self.selenium.find_element(By.XPATH, value='//*[@id="steptree"]//button[@type="submit"]').click()
        self.selenium.find_element(By.ID, value="input-end").click()
        self.selenium.find_element(By.ID, value="confirm-ok").click()

    def test_new_observation_online(self):
        self.login()
        self.go_to_inventory("Test inventory")
        self.go_to_plotobs()
        
        self.assertEqual(
            self.selenium.find_element(By.XPATH, '//*[@id="undermap-commune"]').text,
            "Tramelan"
        )
        self.new_observation()
        self.assertEqual(
            self.selenium.execute_script("return app.currentPlot.obs[%s].year;" % this_year),
            this_year
        )
        # Test new PlotObs/Tree/TreeObs presence
        time.sleep(1)  # FIXME: use some other mean to detect saving is done
        plot = Plot.objects.get(nr=1)
        self.assertIsNotNone(plot.point_exact)
        self.assertIsNotNone(plot.gps_precision)
        self.assertEqual(plot.slope, 30)
        self.assertEqual(plot.plotobs_set.count(), 1)
        po = plot.plotobs_set.get(created__year=this_year)
        self.assertEqual(po.municipality, Municipality.objects.get(name='Tramelan'))
        self.assertEqual(po.inventory, self.inventory)
        self.assertEqual(po.remarks, "Une belle remarque de placette")
        self.assertEqual(po.treeobs_set.count(), 3)
        self.assertEqual(
            sorted(po.treeobs_set.values_list('tree__nr', flat=True)),
            ['1', '2', '3']
        )
        tree1, tree2, tree3 = po.treeobs_set.all()
        self.assertEqual(tree1.vita.code, "m")
        self.assertEqual(tree1.dbh, 14)
        self.assertIs(tree1.marked, True)
        self.assertIsNone(tree1.height)
        self.assertEqual(tree1.remarks, "Remarque.")
        self.assertEqual(tree2.vita.code, "l")
        self.assertEqual(tree2.tree.spec.code, "TA")
        self.assertEqual(tree2.tree.distance, 97)
        self.assertEqual(tree2.tree.azimuth, 280)
        self.assertEqual(tree2.dbh, 27)
        self.assertIs(tree2.marked, False)
        self.assertEqual(tree3.vita.code, "l")
        self.assertEqual(tree3.tree.spec.code, "TA")
        self.assertEqual(tree3.tree.distance, 47)
        self.assertEqual(tree3.tree.azimuth, 380)
        self.assertEqual(tree3.dbh, 14)

    def test_inventory_with_tree_height(self):
        self.inventory.use_tree_height = True
        self.inventory.save()
        self.login()
        self.go_to_inventory(self.inventory.name)
        self.go_to_plotobs()
        self.selenium.find_element(By.CLASS_NAME, 'new').click()
        self.selenium.find_element(By.ID, "input-start").click()
        self.assertIsNone(self.selenium.find_element(By.ID, "tr_height").get_attribute('hidden'))
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step1"]//button[@type="submit"]'))
        ).click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step2"]//button[@type="submit"]'))
        ).click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step3"]//button[@type="submit"]'))
        ).click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="stepfinal"]//button[@class="newtree"]'))
        ).click()
        tree_form = self.selenium.find_element(By.ID, "steptree")
        Select(tree_form.find_element(By.ID, "id_species")).select_by_visible_text("Tanne")
        tree_form.find_element(By.ID, "id_distance").send_keys("24")
        tree_form.find_element(By.ID, "id_azimuth").send_keys("320")
        tree_form.find_element(By.ID, "id_dbh").send_keys("14")
        tree_form.find_element(By.ID, "id_height").send_keys("4")
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="steptree"]//button[@type="submit"]'))
        ).click()
        self.selenium.find_element(By.ID, value="input-end").click()
        self.selenium.find_element(By.ID, value="confirm-ok").click()
        time.sleep(0.5)  # FIXME: use some other mean to detect saving is done
        tree1 = TreeObs.objects.get(tree__nr=1)
        self.assertEqual(tree1.height, 4)

    def test_observation_inaccessible(self):
        self.login()
        self.go_to_inventory("Test inventory")
        self.go_to_plotobs()
        # Click on current year button
        self.selenium.find_element(By.CLASS_NAME, 'new').click()
        self.selenium.find_element(By.ID, "input-start").click()
        ####### STEP 1 ########
        self.assertFalse(self.selenium.find_element(By.ID, "input-premature-end").is_displayed())
        self.assertFalse(self.selenium.find_element(By.ID, "tr_accessible_notes").is_displayed())
        self.selenium.find_element(By.ID, "id_accessible").click()
        self.assertTrue(self.selenium.find_element(By.ID, "tr_accessible_notes").is_displayed())
        self.assertTrue(self.selenium.find_element(By.ID, "input-premature-end").is_displayed())
        self.selenium.find_element(By.ID, "input-premature-end").click()
        self.selenium.find_element(By.ID, "confirm-ok").click()

        time.sleep(1)
        po = PlotObs.objects.get(plot__nr=1)
        self.assertIs(po.accessible, False)


class MobileTests(ProjectDataMixin, TestCase):
    fixtures = ['municipalities_test', 'vita']

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # Existing data
        cls.plotobs = PlotObs.objects.create(
            plot=cls.plot, created=now() - timedelta(days=1000), municipality=cls.municipality,
            year=date.today().year, inventory=Inventory.objects.create(name="Test inv"),
            forest_edgef='1.0'
        )
        tree1 = Tree.objects.create(
            plot=cls.plot, nr=1, azimuth=29, distance=41, spec=TreeSpecies.objects.get(description="Buche"))
        tree2 = Tree.objects.create(
            plot=cls.plot, nr=2, azimuth=229, distance=92, spec=TreeSpecies.objects.get(description="Übrige Laubholz"))
        tree3 = Tree.objects.create(
            plot=cls.plot, nr=3, azimuth=0, distance=0, spec=TreeSpecies.objects.get(description="Tanne"))
        TreeObs.objects.create(
            tree=tree1, obs=cls.plotobs, dbh=25, vita=Vita.objects.get(code='l'),
        )
        TreeObs.objects.create(
            tree=tree2, obs=cls.plotobs, dbh=32, vita=Vita.objects.get(code='l'),
        )
        TreeObs.objects.create(
            tree=tree3, obs=cls.plotobs, dbh=15, vita=Vita.objects.get(code='c'),
        )

    def test_plotobs_json(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("plotobs_detail_json", args=[self.plotobs.pk]))
        self.assertEqual(len(response.json()['features']), 4)

    def test_sync_view(self):
        municipality = Municipality.objects.get(name='Tramelan')
        tree1 = Tree.objects.get(nr=1)
        tree2 = Tree.objects.get(nr=2)
        sync_data = {
            'type': 'FeatureCollection',
            'plot': Plot.objects.get(nr=1).pk,
            'features': [{
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.6962502809437385, 47.53184686184221]},
                'properties': {
                    # Plot-related
                    'exposition': ['S', 'Süden'],
                    'slope': 54,
                    # Plotobs-related
                    'municipality': [str(municipality.pk), 'Tramelan'],
                    'inv_team': str(self.inventory.pk),
                    'year': this_year,
                    'forest_edgef': '1.0',
                    'accessible': 'on',
                    'stand_devel_stage': ['', '---------'],
                    'forest_form': ['', '---------'],
                    'remarks': '',
                    'type': 'center',
                }
            }, {
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.696274279412243, 47.53188017026505]},
                'properties': {
                    'nr': '1',
                    'species': [str(tree1.spec_id), 'Buche'],
                    'vita': ['1', 'Baum ist seit der letzten Aufnahme genutzt wurden. (c)'],
                    'distance': '41.00',
                    'azimuth': '29',
                    'tree': str(tree1.pk),
                    'dbh': '0',
                },
            }, {
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.696196430721728, 47.531772120990965]},
                'properties': {
                    'nr': '2',
                    'species': [str(tree2.spec_id), 'Autre feuillu'],
                    'vita': ['3', 'Baum lebt. (l)'],
                    'distance': '92.00',
                    'azimuth': '229',
                    'tree': str(tree2.pk),
                    'dbh': '32',
                },
            }, {
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.6962199123552235, 47.53185357109485]},
                'properties': {
                    'nr': '3',
                    'species': [str(tree2.spec_id), 'Autre feuillu'],
                    'vita': ['2', 'Baum ist seit der letzten Aufnahme neu eingewachsen. (e)'],
                    'distance': '24',
                    'azimuth': '320',
                    'tree': '',
                    'dbh': '14',
                },
            }]
        }
        self.client.force_login(self.user)
        response = self.client.post(
            reverse("mobile-sync"), json.dumps(sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        po = self.plot.plotobs_set.get(created__year=date.today().year)
        self.assertEqual(po.treeobs_set.count(), 3)
