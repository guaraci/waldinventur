from settings.base import *  # NOQA


def gettext_noop(s):
    return s


INSTALLED_APPS.extend([
    'waldinventur',
    'mobile_base',
    'mobile',
])

MIDDLEWARE.insert(
    MIDDLEWARE.index('django.middleware.common.CommonMiddleware'), 'django.middleware.locale.LocaleMiddleware'
)

MAIN_APP = 'waldinventur'

LANGUAGES = [
    ("de", "Deutsch"),
    ("fr-ch", "Français"),
]

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'waldinventur',
        'USER': '',
        'PASSWORD': '',
        'OPTIONS': {
            'options': '-c search_path=public,postgis'
        },
    },
}

AUTH_USER_MODEL = "waldinventur.User"

DEFAULT_LON = 2575000
DEFAULT_LAT = 1232500

MOBILE_TITLE = gettext_noop("Waldinventur")
