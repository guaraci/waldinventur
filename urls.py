from django.urls import include, path

from mobile.views import main
from . import views

urlpatterns = [
    path('overview_map/', views.OverviewMap.as_view(), name="overview-map"),
    path('overview_map/data/', views.OverviewMapData.as_view(), name="overview-map-data"),
    path('manage/', views.ManageView.as_view(), name='manage'),
    path('', include('mobile.urls')),
    path('', main, name='home'),
]
