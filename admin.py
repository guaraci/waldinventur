from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.gis.db import models as gis_models

from observation.admin import MyOSMWidget, color_colored
from . import models


@admin.register(models.User)
class UserAdmin(DjangoUserAdmin):
    pass


@admin.register(models.Municipality)
class MunicipalityAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Perimeter)
class PerimeterAdmin(admin.ModelAdmin):
    list_display = ['owner', 'devel_stage']


@admin.register(models.Inventory)
class InventoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'inv_from', 'inv_to']
    formfield_overrides = {
        gis_models.PolygonField: {'widget': MyOSMWidget},
    }


@admin.register(models.Plot)
class PlotAdmin(admin.ModelAdmin):
    list_display = ['nr', 'sealevel', 'slope', 'center']
    formfield_overrides = {
        gis_models.PointField: {'widget': MyOSMWidget},
    }


@admin.register(models.PlotObs)
class PlotObsAdmin(admin.ModelAdmin):
    list_display = ['created', 'plot', 'year', 'municipality']
    search_fields = ['plot__nr']
    raw_id_fields = ['plot']


@admin.register(models.TreeSpecies)
class TreeSpeciesAdmin(admin.ModelAdmin):
    list_display = ['description', 'description_fr', 'code', color_colored, 'order']


@admin.register(models.Tree)
class TreeAdmin(admin.ModelAdmin):
    list_display = ['nr', 'spec', 'azimuth', 'distance']


@admin.register(models.Vita)
class VitaAdmin(admin.ModelAdmin):
    list_display = ['code', 'description', 'description_fr']


@admin.register(models.TreeObs)
class TreeObsAdmin(admin.ModelAdmin):
    list_display = ['dbh', 'remarks']
    raw_id_fields = ['obs', 'tree']


@admin.register(models.DevelStage)
class DevelStageAdmin(admin.ModelAdmin):
    list_display = ['code', 'description', 'description_fr']
