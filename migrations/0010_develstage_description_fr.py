from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0009_french_vita_species'),
    ]

    operations = [
        migrations.AddField(
            model_name='develstage',
            name='description_fr',
            field=models.CharField(blank=True, max_length=100, verbose_name='Beschreibung (fr)'),
        ),
    ]
