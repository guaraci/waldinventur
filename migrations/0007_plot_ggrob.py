from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0006_inventory_managers'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='ggrob',
            field=models.CharField(blank=True, max_length=8),
        ),
    ]
