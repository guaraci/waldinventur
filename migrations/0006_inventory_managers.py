from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0005_plotobs_inventory'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='managers',
            field=models.ManyToManyField(blank=True, related_name='inventories_as_lead', to=settings.AUTH_USER_MODEL, verbose_name='Projektleiter'),
        ),
        migrations.AlterField(
            model_name='inventory',
            name='operators',
            field=models.ManyToManyField(blank=True, related_name='inventories_as_op', to=settings.AUTH_USER_MODEL, verbose_name='Aufnahmeleiter'),
        ),
    ]
