from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0008_plot_nais_typ_6_chars'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='description_fr',
            field=models.CharField(blank=True, max_length=100, verbose_name='Beschreibung (fr)'),
        ),
        migrations.AddField(
            model_name='vita',
            name='description_fr',
            field=models.CharField(blank=True, max_length=100, verbose_name='Beschreibung (fr)'),
        ),
    ]
