from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import observation.model_utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.permission', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'auth_user',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='DevelStage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=30, unique=True, verbose_name='Code')),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_devel_stage',
            },
        ),
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Inventar')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'inventory',
            },
        ),
        migrations.CreateModel(
            name='Municipality',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('bfs_nr', models.IntegerField(null=True)),
                ('plz', models.CharField(blank=True, max_length=4)),
                ('kanton', models.CharField(max_length=2)),
                ('perim', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'municipality',
            },
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15, verbose_name='Plotnummer')),
                ('center', django.contrib.gis.db.models.fields.PointField(srid=2056, verbose_name='Plotcenter')),
                ('point_exact', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=2056)),
                ('gps_precision', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True, verbose_name='GPS Genauigkeit [m]')),
                ('sealevel', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Höhe')),
                ('slope', models.SmallIntegerField(blank=True, null=True, verbose_name='Neigung')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('nais_typ', models.CharField(blank=True, max_length=4)),
                ('lfi_bez', models.CharField(blank=True, max_length=4)),
            ],
            options={
                'db_table': 'plot',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Erstellt am')),
                ('year', models.PositiveSmallIntegerField(db_index=True, verbose_name='Aufnahmejahr')),
                ('forest_edgef', models.DecimalField(decimal_places=1, max_digits=2, verbose_name='Waldrandfaktor')),
                ('accessible', models.BooleanField(default=True, verbose_name='Zugänglicher Wald')),
                ('accessible_notes', models.TextField(blank=True, verbose_name='Zugangbemerkungen')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('devel_stage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='waldinventur.develstage', verbose_name='Entwicklungstufe')),
                ('municipality', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='waldinventur.municipality', verbose_name='Gemeinde')),
                ('operator', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Operator')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='waldinventur.plot', verbose_name='Plot')),
            ],
            options={
                'db_table': 'plot_obs',
            },
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15, verbose_name='Nummer')),
                ('azimuth', models.PositiveSmallIntegerField(verbose_name='Grad (400)')),
                ('distance', models.PositiveSmallIntegerField(verbose_name='in Dezimeter [dm]')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='waldinventur.plot')),
            ],
            options={
                'db_table': 'tree',
            },
            bases=(observation.model_utils.TreeMixin, models.Model),
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=7)),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('order', models.PositiveSmallIntegerField(default=0, verbose_name='Sortierung')),
                ('tarif_klass20', models.CharField(blank=True, choices=[('epicea', 'Fichte (HAHBART1)'), ('sapin', 'Tanne (HAHBART2)'), ('hetre', 'Buche (HAHBART7)'), ('autres_feu', 'übrige Laubholz (HAHBART12)'), ('autres_res', 'übrige Nadelholz (HAHBART6)')], max_length=15)),
            ],
            options={
                'db_table': 'lt_tree_spec',
            },
        ),
        migrations.CreateModel(
            name='Vita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1, unique=True, verbose_name='Code')),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_vita',
            },
        ),
        migrations.CreateModel(
            name='TreeObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dbh', models.PositiveSmallIntegerField(verbose_name='BHD [cm]')),
                ('height', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Baumhöhe [m]')),
                ('marked', models.BooleanField(default=False, verbose_name='Markiert')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('obs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='waldinventur.plotobs')),
                ('tree', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='waldinventur.tree', verbose_name='Baum')),
                ('vita', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='waldinventur.vita', verbose_name='Lebenslauf')),
            ],
            options={
                'db_table': 'tree_obs',
            },
        ),
        migrations.AddField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='waldinventur.treespecies', verbose_name='Baumart'),
        ),
        migrations.CreateModel(
            name='Perimeter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner', models.CharField(max_length=25)),
                ('poly', django.contrib.gis.db.models.fields.MultiPolygonField(srid=2056)),
                ('devel_stage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='waldinventur.develstage')),
            ],
            options={
                'db_table': 'perimeter',
            },
        ),
        migrations.AddConstraint(
            model_name='treeobs',
            constraint=models.UniqueConstraint(fields=('obs', 'tree'), name='unique_tree_per_plot_obs'),
        ),
        migrations.AddConstraint(
            model_name='plotobs',
            constraint=models.CheckConstraint(
                condition=models.Q(('forest_edgef__gte', 0), ('forest_edgef__lte', 1)),
                name='forest_edgef_betw_0_1'
            ),
        ),
    ]
