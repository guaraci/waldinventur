from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0002_treespecies_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='inv_from',
            field=models.DateField(null=True, verbose_name='Von'),
        ),
        migrations.AddField(
            model_name='inventory',
            name='inv_to',
            field=models.DateField(null=True, verbose_name='Bis'),
        ),
    ]
