from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0010_develstage_description_fr'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='use_tree_height',
            field=models.BooleanField(default=False, verbose_name='Baumhöhe verwenden'),
        ),
    ]
