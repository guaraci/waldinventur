import colorful.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='color',
            field=colorful.fields.RGBColorField(blank=True, verbose_name='Farbe'),
        ),
    ]
