from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0004_inventory_operators'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='inventory',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to='waldinventur.inventory', verbose_name='Inventory'),
        ),
    ]
