from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldinventur', '0003_inventory_inv_from_inv_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='operators',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL, verbose_name='Betreiber'),
        ),
    ]
