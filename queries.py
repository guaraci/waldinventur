from django.utils.translation import gettext as _

from observation.queries import Groupment, GroupmentSpec, GroupmentSpecGroup


class PerimeterSet:
    @classmethod
    def get_perimeters(cls):
        return []


def get_groupments():
    return {
        'perims': {
            'name': 'paggr',
            'type': 'radio',
            'deselectable': True,
            'groupments': [
                Groupment('municipality', _('Gemeinde')),
            ]
        },
        'periods': {
            'name': 'yaggr',
            'type': 'radio',
            'groupments': [
                Groupment('year', 'Jahr', checked=True),
            ]
        },
        'various': {
            'name': 'aggr',
            'type': 'checkbox',
            'view_dependant': True,
            'groupments': [
                GroupmentSpec('spec', "Baumart"),
                GroupmentSpecGroup('group', "Baumartgruppe"),
            ]
        },
    }


VIEW_MAP = {}
