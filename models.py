import json
from datetime import date

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models.functions import Transform
from django.db import models
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import get_language, gettext_lazy as _

from colorful.fields import RGBColorField

from observation.model_utils import InventoryBase, PlotBase, PlotObsBase, TreeMixin


# Add some fake classes to allow running the project
class AdminRegion:
    pass
class Owner:
    pass
class OwnerType:
    pass
class TreeGroup:
    pass


class User(AbstractUser):
    class Meta:
        db_table = 'auth_user'

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return super().get_full_name() or self.username


class Inventory(InventoryBase):
    name = models.CharField(_("Inventar"), max_length=150)
    geom = gis_models.PolygonField(srid=2056, blank=True, null=True)
    inv_from = models.DateField(_("Von"), null=True)
    inv_to = models.DateField(_("Bis"), null=True)
    use_tree_height = models.BooleanField(_("Baumhöhe verwenden"), default=False)
    managers = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, verbose_name=_("Projektleiter"),
        related_name="inventories_as_lead",
    )
    operators = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, verbose_name=_("Aufnahmeleiter"),
         related_name="inventories_as_op",
    )

    excluded_plots = ()
    new_plots_allowed = False

    class Meta:
        db_table = 'inventory'

    def __str__(self):
        return self.name

    def edit_url(self):
        return reverse('admin:waldinventur_inventory_change', args=[self.pk])

    def dates_str(self):
        if self.inv_from.year == self.inv_to.year:
            return f"{self.inv_from.strftime('%d.%m')} - {self.inv_to.strftime('%d.%m.%Y')}"
        return f"{self.inv_from.strftime('%d.%m.%Y')} ­- {self.inv_to.strftime('%d.%m.%Y')}"

    @property
    def years(self):
        return list(range(self.inv_from.year, self.inv_to.year + 1))

    @classmethod
    def get_for_manager(cls, user):
        return Inventory.objects.all() if user.is_superuser else user.inventories_as_lead.all()

    @classmethod
    def get_for_mobile(cls, user):
        inventories = cls.objects.filter(inv_from__lte=date.today(), inv_to__gte=date.today())
        if not user.is_staff:
            inventories = inventories.filter(operators=user)
        return inventories

    @classmethod
    def get_from_plot(cls, plot):
        return Inventory.objects.get(geom__contains=plot.center)

    def get_plots(self, srid=None):
        plot_qs = Plot.objects.filter(
            center__within=self.geom
        ).annotate(
            plotgeom=Transform('center', srid), plotgeomexact=Transform('point_exact', srid)
        )
        return plot_qs

    def exclude_plots(self, plot_ids):
        to_exclude = self.get_plots(2056).filter(pk__in=plot_ids).annotate(
            numobs=models.Count('plotobs')
        ).filter(numobs=0)
        to_exclude.delete()

    def get_plotobs_from_previous_inv(self):
        return PlotObs.objects.none()

    def layer_urls(self):
        return {}

    def as_geojson(self, srid=None):
        if (srid is not None and srid != self.geom.srid):
            self.geom.transform(srid)
        geojson = {
            "type": "Feature",
            "id": self.pk,
            "properties": {
                "id": self.pk,
                "name": self.name,
            },
            "geometry": json.loads(self.geom.json),
        }
        return geojson


class Municipality(models.Model):
    name = models.CharField(max_length=50, unique=True)
    bfs_nr = models.IntegerField(null=True)
    plz = models.CharField(max_length=4, blank=True)
    kanton = models.CharField(max_length=2)
    perim = gis_models.MultiPolygonField(srid=2056, blank=True, null=True)

    geom_field = 'perim'

    class Meta:
        db_table = 'municipality'

    def __str__(self):
        return self.name

    @classmethod
    def get_from_plot(cls, plot):
        try:
            return Municipality.objects.get(perim__contains=plot.center)
        except Municipality.DoesNotExist:
            return None

    def as_geojson(self, srid=None):
        geom = self.perim
        if (srid is not None and srid != geom.srid):
            geom.transform(srid)

        geojson = {
            "type": "Feature",
            "id": self.pk,
            "crs": {"type": "name", "properties": {"name": "urn:x-ogc:def:crs:EPSG:%s" % geom.srid}},
            "properties": {
                "id": self.pk,
                "name": self.name,
                "bfs_nr": self.bfs_nr,
                "plz": self.plz,
                "kanton": self.kanton,
            },
            "geometry": json.loads(geom.json),
        }
        return geojson


class Plot(PlotBase):
    nr = models.CharField(_("Plotnummer"), max_length=15)
    center = gis_models.PointField(_("Plotcenter"), srid=2056)
    point_exact = gis_models.PointField(srid=2056, blank=True, null=True)
    gps_precision = models.DecimalField(
        _("GPS Genauigkeit [m]"), max_digits=8, decimal_places=2, blank=True, null=True
    )
    sealevel = models.PositiveSmallIntegerField(_("Höhe"), blank=True, null=True)
    slope = models.SmallIntegerField(_("Neigung"), null=True, blank=True)
    remarks = models.TextField(_("Bemerkungen"), blank=True)
    # Data from LFI:
    nais_typ = models.CharField(max_length=6, blank=True)
    lfi_bez = models.CharField(max_length=4, blank=True)
    ggrob = models.CharField(max_length=8, blank=True)

    geom_field = 'center'

    def __str__(self):
        return self.nr

    def get_observations(self):
        return self.plotobs_set.prefetch_related('treeobs_set').order_by('created')

    def populate_from_feature(self, layer, feature):
        for fname in ['nais_typ', 'lfi_bez', 'ggrob']:
            if fname in layer.fields:
                setattr(self, fname, feature.get(fname) or '')

    def as_geojson(self, dumped=True, srid=None, geom_field='center', for_mobile=False):
        geom = getattr(self, geom_field)
        if (srid is not None and srid != geom.srid):
            geom.transform(srid)
        urlname = 'mobile_plotobs_detail_json' if for_mobile else 'plotobs_detail_json'
        perimeter = Perimeter.from_point(geom)
        geojson = {
            "type": "Feature",
            "id": self.pk,
            "properties": {
                "id": self.pk,
                "nr": self.nr,
                "slope": self.slope,
                "sealevel": self.sealevel,
                "radius": self.radius,
                # initial values for devel_stage and forest_form
                "devel_stage": [perimeter.devel_stage_id, ''] if perimeter else ['', ''],
                "obsURLs": {str(obs.year): reverse(urlname, args=[obs.pk]) for obs in self.plotobs_set.all()},
                "plotData": [
                    {"field": "nr", "label": _("Nr"), "showEmpty": True},
                    {"field": "sealevel", "label": _("Höhe"), "unit": "m", "showEmpty": True},
                    {"field": "slope", "label": _("Neigung"), "unit": "%", "showEmpty": True},
                    {"field": "radius", "label": _("Radius"), "unit": "m", "showEmpty": True},
                ],
            },
            "geometry": json.loads(geom.json),
        }
        return geojson


class DevelStage(models.Model):
    code = models.CharField(_("Code"), max_length=30, unique=True)
    description = models.CharField(_("Beschreibung"), max_length=100)
    description_fr = models.CharField(_("Beschreibung (fr)"), max_length=100, blank=True)

    class Meta:
        db_table = 'lt_devel_stage'

    def __str__(self):
        if get_language().startswith('fr') and self.description_fr:
            return self.description_fr
        return self.description


class PlotObs(PlotObsBase):
    plot = models.ForeignKey(Plot, verbose_name=_("Plot"), on_delete=models.CASCADE)
    municipality = models.ForeignKey(
        Municipality, blank=True, null=True, on_delete=models.PROTECT, verbose_name=_("Gemeinde")
    )
    year = models.PositiveSmallIntegerField(_("Aufnahmejahr"), db_index=True)
    inventory = models.ForeignKey(
        Inventory, blank=True, null=True, on_delete=models.PROTECT, verbose_name=_("Inventory")
    )
    forest_edgef = models.DecimalField(_("Waldrandfaktor"), max_digits=2, decimal_places=1)
    accessible = models.BooleanField(_("Zugänglicher Wald"), default=True)
    accessible_notes = models.TextField(_("Zugangbemerkungen"), blank=True)
    devel_stage = models.ForeignKey(
        DevelStage, on_delete=models.PROTECT, blank=True, null=True, verbose_name=_("Entwicklungstufe")
    )
    remarks = models.TextField(_("Bemerkungen"), blank=True)

    inventory_field = 'inventory'

    class Meta:
        db_table = 'plot_obs'
        constraints = [
            models.CheckConstraint(
                condition=models.Q(forest_edgef__gte=0, forest_edgef__lte=1),
                name='forest_edgef_betw_0_1'
            ),
        ]

    @classmethod
    def init_kwargs_from_mobile_data(cls, plot, data):
        """Return PlotObs instance from data received from mobile forms."""
        municipality = Municipality.get_from_plot(plot)
        return {
            'municipality': municipality,
        }

    def visible_fields(self, exclude_empty=True):
        return [
            field.name
            for field in self._meta.fields
            if (field.name not in ('id', 'plot')
                and (not exclude_empty or getattr(self, field.name) not in (None, '')))
        ]

    def previous_obs(self):
        return self.plot.plotobs_set.order_by('created').last() if self.plot.pk else None

    def as_geojson(self, **kwargs):
        data = super().as_geojson(**kwargs)
        data['features'][0]['properties']['year'] = self.created.year
        return data


class Vita(models.Model):
    code = models.CharField(_("Code"), max_length=1, unique=True)
    description = models.CharField(_("Beschreibung"), max_length=100)
    description_fr = models.CharField(_("Beschreibung (fr)"), max_length=100, blank=True)

    class Meta:
        db_table = 'lt_vita'

    def __str__(self):
        if get_language().startswith('fr') and self.description_fr:
            descr = self.description_fr
        else:
            descr = self.description
        return f"{descr} ({self.code})"


class TreeSpecies(models.Model):
    TARIFKLASS_CHOICES = (
        ('epicea', _('Fichte (HAHBART1)')),
        ('sapin', _('Tanne (HAHBART2)')),
        ('hetre', _('Buche (HAHBART7)')),
        ('autres_feu', _('übrige Laubholz (HAHBART12)')),
        ('autres_res', _('übrige Nadelholz (HAHBART6)')),
    )
    code = models.CharField(max_length=7)
    description = models.CharField(_("Beschreibung"), max_length=100)
    description_fr = models.CharField(_("Beschreibung (fr)"), max_length=100, blank=True)
    order = models.PositiveSmallIntegerField(_("Sortierung"), default=0)
    color = RGBColorField(_("Farbe"), blank=True)
    tarif_klass20 = models.CharField(max_length=15, choices=TARIFKLASS_CHOICES, blank=True)

    name_field = 'description'

    class Meta:
        db_table = 'lt_tree_spec'

    def __str__(self):
        if get_language().startswith('fr') and self.description_fr:
            return self.description_fr
        return self.description

    @property
    def abbrev(self):
        return self.code

    @classmethod
    def active_species(cls):
        return TreeSpecies.objects.all().order_by('order')


class Tree(TreeMixin, models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE)
    nr = models.CharField(_("Nummer"), max_length=15)
    spec = models.ForeignKey(TreeSpecies, blank=True, null=True, on_delete=models.PROTECT, verbose_name=_("Baumart"))
    azimuth = models.PositiveSmallIntegerField(_("Grad (400)"))
    distance = models.PositiveSmallIntegerField(_("in Dezimeter [dm]"))

    class Meta:
        db_table = 'tree'


class TreeObs(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE, verbose_name=_("Baum"))
    dbh = models.PositiveSmallIntegerField(_("BHD [cm]"))
    vita = models.ForeignKey(Vita, verbose_name=_("Lebenslauf"), on_delete=models.PROTECT)
    height = models.PositiveSmallIntegerField(_("Baumhöhe [m]"), null=True, blank=True)
    marked = models.BooleanField(_("Markiert"), default=False)
    remarks = models.TextField(_("Bemerkungen"), blank=True)

    class Meta:
        db_table = 'tree_obs'
        constraints = [
            models.UniqueConstraint(fields=['obs', 'tree'], name='unique_tree_per_plot_obs')
        ]


class Perimeter(models.Model):
    owner = models.CharField(max_length=25)
    devel_stage = models.ForeignKey(DevelStage, on_delete=models.PROTECT, blank=True, null=True)
    poly = gis_models.MultiPolygonField(srid=2056)

    class Meta:
        db_table = 'perimeter'

    @classmethod
    def from_point(cls, pt):
        return cls.objects.filter(poly__contains=pt).first()
